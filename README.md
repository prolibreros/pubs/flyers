# _Flyers_ sobre FOSS

Estos _flyers_ son hechos para imprimirlos y compartirlos.
Sus temas son sobre las conveniencias en el uso de _software_
libre o de código abierto (FOSS, por sus siglas en inglés).

## _Descarga Jitsi Meet_

![](01-jitsi/flyer_color.png)

[Fuentes](https://pad.programando.li/hacklab-charlas-nuestras-redes#1-%C2%BFQu%C3%A9-hay-detr%C3%A1s-de-las-videollamadas-La-alternativa-de-Jitsi-frente-a-Zoom-y-otras-mierdas)

## _#NiCensuraNiCandados_

![](02-lfda/flyer_color.png)

[Más info](https://pad.programando.li/programando-flyer2)

## La Netflix…

![](03-netflix/flyer_color.png)

[Más info](https://pad.programando.li/programando-flyer3)

## Licencias

* Cómics: hechas por [Antílope](https://www.instagram.com/aantilopee) y con licencia [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.es).
* Textos (redacción que no forma parte del cómic): hechas por Programando LIBREros y con [Licencia Editorial Abierta y Libre](https://gitlab.com/programando-libreros/juridico/licencia-editorial-abierta-y-libre).
* Fuentes: hechas por terceros y con [Open Font License](https://theleagueof.github.io/licenses).
